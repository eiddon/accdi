package net.citrusbytes.accdi.util;

public class UnicodeCharacter {

	private String c;
	
	public UnicodeCharacter(String c) throws NotACharacterException {
		if(c == null || c.length() != 1)
			throw new NotACharacterException(String.format("%s is not a unicode character!"));
		this.c = c;
	}
	
	@Override
	public String toString() {
		return c;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof UnicodeCharacter) {
			UnicodeCharacter other = (UnicodeCharacter)obj;
			return other.c.equals(c);
		}
		
		return false;
	}
	
	@Override
	public int hashCode() {
		return c.hashCode();
	}
	
}
