package net.citrusbytes.accdi.util;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class StringUtils {

	public static List<UnicodeCharacter> tapeFromString(String tape) throws NotACharacterException {
		List<UnicodeCharacter> l = new ArrayList<>();
		if(tape == null)
			return l;
		for(int i = 0; i < tape.length(); i++) {
			l.add(new UnicodeCharacter(tape.substring(i, i+1)));
		}
		return l;
	}
	
	public static Set<UnicodeCharacter> alphabetFromString(String alphabet) throws NotACharacterException {
		Set<UnicodeCharacter> s = new HashSet<>();
		List<UnicodeCharacter> l = tapeFromString(alphabet);
		for(UnicodeCharacter uc : l)
			s.add(uc);
		return s;
	}
	
}
