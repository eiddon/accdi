package net.citrusbytes.accdi.turing;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.gson.Gson;

import net.citrusbytes.accdi.json.TuringSpecifier;
import net.citrusbytes.accdi.util.NotACharacterException;
import net.citrusbytes.accdi.util.StringUtils;
import net.citrusbytes.accdi.util.UnicodeCharacter;

public class TuringMachine {

	private Tape tape;
	private Set<UnicodeCharacter> alphabet;
	private UnicodeCharacter blank;
	private Set<State> states;
	private Set<State> accepts;
	private Set<State> rejects;
	private State state;
	private Transition transition;

	public TuringMachine(Set<UnicodeCharacter> alphabet,
			List<UnicodeCharacter> tape, UnicodeCharacter blank,
			Set<State> states, Set<State> accepts, Set<State> rejects,
			State start, Transition transition) throws ClassInvariantException {
		this.alphabet = alphabet;
		this.tape = new Tape(tape, blank);
		this.blank = blank;
		this.states = states;
		this.accepts = accepts;
		this.rejects = rejects;
		this.state = start;
		this.transition = transition;
		enforceInvariants();

	}

	private void enforceInvariants() throws ClassInvariantException {
		// Enforce Invariants
		for (UnicodeCharacter uc : tape)
			asserts(this.alphabet.contains(uc) || uc.equals(blank),
					String.format(
							"All tape symbols must be in the alphabet; %s is not.",
							uc));
		asserts((this.states != null && this.states.size() > 0),
				"You must have states.");
		for (State s : accepts)
			asserts(this.states.contains(s), "All accepts must be in states.");
		for (State s : rejects)
			asserts(this.states.contains(s), "All rejects must be in states.");
		asserts(this.state != null, "You must be in a starting state.");
		asserts(this.transition != null || this.accepts.contains(state)
				|| this.rejects.contains(state), "You must have transitions.");
		asserts(this.blank != null, "You need a blank character.");
	}
	
	public TuringMachine(String file) throws ClassInvariantException {
		try (FileInputStream fis = new FileInputStream(new File(file))){
			if(file == null)
				throw new ClassInvariantException("JSON specifier must exist.");
			TuringSpecifier ts = load(fis, TuringSpecifier.class);
			setFieldsFromSpecifier(ts);
		} catch (FileNotFoundException e) {
			throw new ClassInvariantException("JSON specifier must exist.");
		} catch (IOException e) {
			throw new ClassInvariantException("IO Error on JSON specifier. Can you read the file?");
		}
	}
	
	/**
	 * @author Guillaume Polet http://stackoverflow.com/questions/9566823/load-an-object-using-gson
	 */
    public static <T> T load(final InputStream inputStream, final Class<T> clazz) {
        try {
            if (inputStream != null) {
                final Gson gson = new Gson();
                final BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
                return gson.fromJson(reader, clazz);
            }
        } catch (final Exception e) {
        	
        }
        return null;
    }

	public TuringMachine(TuringSpecifier ts) throws ClassInvariantException {
		setFieldsFromSpecifier(ts);
	}
	
	private void setFieldsFromSpecifier(TuringSpecifier ts) throws ClassInvariantException {
		try {
			
			alphabet = StringUtils.alphabetFromString(ts.alphabet);
			blank = new UnicodeCharacter(ts.blank);
			tape = new Tape(StringUtils.tapeFromString(ts.tape), blank);
			states = new HashSet<State>();
			for (String s : ts.states) {
				states.add(new State(s));
			}

			accepts = new HashSet<State>();
			for (String s : ts.accepts) {
				accepts.add(new State(s));
			}

			rejects = new HashSet<State>();
			for (String s : ts.rejects) {
				rejects.add(new State(s));
			}

			state = new State(ts.start);

			Delta[] ds = new Delta[ts.deltas.length];
			for (int i = 0; i < ds.length; i++) {
				String[] spec = ts.deltas[i];
				if (spec.length != 5)
					throw new ClassInvariantException(
							String.format("Invalid delta specification. Please ensure you have 5 elements for each spec."));
				ds[i] = new Delta(new State(spec[0]), new UnicodeCharacter(
						spec[1]), new State(spec[2]), new UnicodeCharacter(
						spec[3]), (spec[4].equals("R") ? Direction.RIGHT
						: Direction.LEFT));
			}
			
			transition = new Transition(ds);

		} catch (NotACharacterException e) {
			throw new ClassInvariantException(e);
		} catch (NullPointerException e) {
			throw new ClassInvariantException("Missing field", e);
		}
	}

	private void asserts(boolean b, String fail) throws ClassInvariantException {
		if (!b)
			throw new ClassInvariantException(fail);
	}

	public TuringMachineRuntimeDescriptor step() throws IncompleteTransitionFunctionException {
		if (isHalted())
			return encode();
		Instruction i = transition.step(state, tape.scan());
		tape.doInstruction(i);
		state = i.newState;
		return encode();
	}

	public TuringMachineRuntimeDescriptor encode() {
		StringBuilder sb = new StringBuilder();
		for(UnicodeCharacter uc : tape) {
			sb.append(uc.toString());
		}
		return new TuringMachineRuntimeDescriptor(sb.toString(), state.toString(), tape.getScanHead());
	}

	public boolean isHalted() {
		return (accepts.contains(state) || rejects.contains(state));
	}
	
	public UnicodeCharacter getBlank() {
		return blank;
	}

}
