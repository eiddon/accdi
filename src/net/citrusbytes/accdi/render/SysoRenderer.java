package net.citrusbytes.accdi.render;

import net.citrusbytes.accdi.turing.TuringMachineRuntimeDescriptor;

public class SysoRenderer implements Renderer {

	private boolean closed;
	
	public SysoRenderer() {
		closed = false;
	}
	
	@Override
	public void render(TuringMachineRuntimeDescriptor tmrd)
			throws AlreadyClosedException {
		if(closed)
			throw new AlreadyClosedException();
		System.out.println(tmrd);
	}

	@Override
	public void close() {
		closed = true;
	}

}
