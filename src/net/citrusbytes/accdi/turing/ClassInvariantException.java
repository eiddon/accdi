package net.citrusbytes.accdi.turing;


public class ClassInvariantException extends Exception {

	private static final long serialVersionUID = 1801380983843224628L;

	public ClassInvariantException(String msg) {
		super(msg);
	}

	public ClassInvariantException(Throwable cause) {
		super(cause);
	}

	public ClassInvariantException(String msg, Exception cause) {
		super(msg, cause);
	}
}
