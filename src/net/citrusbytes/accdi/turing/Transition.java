package net.citrusbytes.accdi.turing;

import java.util.HashSet;
import java.util.Set;

import net.citrusbytes.accdi.util.UnicodeCharacter;

public class Transition {

	private Set<Delta> ds;
	
	public Transition(Delta... ds) {
		this.ds = new HashSet<Delta>();
		for(Delta d : ds)
			this.ds.add(d);
		if(ds.length != this.ds.size())
			System.out.println("WARN you had colliding deltas");
	}
	
	public Instruction step(State state, UnicodeCharacter symbol) throws IncompleteTransitionFunctionException {
		Delta half = new Delta(state, symbol, null, null, null);
		Delta ret = null;
		for(Delta d : ds) {
			if(half.equals(d)) {
				ret = d;
				break;
			}
		}
		
		if(ret == null)
			throw new IncompleteTransitionFunctionException(String.format("No delta found for state:%s and symbol:%s", state, symbol));
		return new Instruction(ret.dout, ret.cout, ret.sout);
	}
	
}
