package net.citrusbytes.accdi.turing;

import net.citrusbytes.accdi.util.UnicodeCharacter;

public class Instruction {
	
	public final Direction d;
	public final UnicodeCharacter symbol;
	public final State newState;
	
	public Instruction(Direction direction, UnicodeCharacter symbol, State newState) {
		this.d = direction;
		this.symbol = symbol;
		this.newState = newState;
	}
	
}
