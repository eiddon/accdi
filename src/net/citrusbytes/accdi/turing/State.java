package net.citrusbytes.accdi.turing;

public class State {

	private String name;
	
	public State(String name) {
		this.name = name;
	}
	
	@Override
	public String toString() {
		return name;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(name == null)
			return false;
		if(obj instanceof State) {
			State other = (State)obj;
			return name.equals(other.name);
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		return name.hashCode();
	}
	
}