package net.citrusbytes.accdi.render;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

import net.citrusbytes.accdi.turing.TuringMachineRuntimeDescriptor;
import net.citrusbytes.accdi.util.UnicodeCharacter;

public class LatexFileRenderer implements Renderer {

	private PrintWriter f;
	private boolean closed;
	private boolean tape;
	private boolean conf;
	
	public LatexFileRenderer(String filePath, boolean tape, boolean conf) throws FileNotFoundException {
		f = new PrintWriter(new FileOutputStream(new File(filePath)));
		f.println("%How to use this file");
		f.println("%Make sure your document contains the following in preamble:");
		f.println("%\t \\usepackage{tikz}");
		f.println("%\t \\usetikzlibrary{positioning,calc}");
		f.println("%Then either copy-paste(ew!) the contents or use \\input{file} (without the .tex extension)");
		f.println("\\begin{center}");
		closed = false;
		this.tape = tape;
		this.conf = conf;
	}
	
	@Override
	public void render(TuringMachineRuntimeDescriptor tmrd)
			throws AlreadyClosedException {
		if(closed)
			throw new AlreadyClosedException();
		if(tape) {
			f.println("\\begin{tikzpicture}[every node/.style={block}, block/.style={minimum height=1.5em,outer sep=0pt,draw,rectangle,node distance=0pt}]");
			int i = 0;
			char c = tmrd.tape.charAt(i);
			String s = (c == ' ')?"\\textvisiblespace":Character.toString(c);
			f.println(String.format("\\node (%d) {$%s$};", i, s));
			for(i++; i < tmrd.tape.length(); i++) {
				c = tmrd.tape.charAt(i);
				s = (c == ' ')?"\\textvisiblespace":Character.toString(c);
				f.println(String.format("\\node (%d) [right=of %d] {$%s$};",i,i-1,s));
			}
			f.println(String.format("\\node (%d) [right=of %d] {$\\textvisiblespace$};", i,i-1));
			i++;
			f.println(String.format("\\node (%d) [right=of %d] {$\\cdots$};", i,i-1));
			i++;
			f.println(String.format("\\node (%d) [above = 0.25cm of %d,draw=red,thick] {$%s$};",i,tmrd.headLocation,tmrd.state));
			f.println(String.format("\\draw[-latex] (%d) -- (%d);",i,tmrd.headLocation));
			f.println("\\end{tikzpicture}");
			f.println("\\linebreak");
		}

		if(conf){
			f.print(tmrd.tape.substring(0,tmrd.headLocation));
			f.print(tmrd.state);
			f.println(tmrd.tape.substring(tmrd.headLocation, tmrd.tape.length()));
			f.println("\\linebreak");
		}


	}

	@Override
	public void close() {
		f.println("\\end{center}");
		f.close();
		closed = true;
	}

}
