A Computer Could Do It
====

Why?
===
I don't want to manually trace a turing machine's execution because a computer could do it!
Computers also have the patience to draw diagrams which is always nice.

What?
===
 * Traces execution of a turing machine specified by an input file (see example.json for an example).
 * Draws pictures of the trace into LaTeX source using tikz.

Installing
===
 * Make sure you have [Apache ant](http://ant.apache.org/) installed.
 * Make sure you have [Apache ivy](http://ant.apache.org/ivy/download.cgi) installed.
 Then simply execute ant in the directory while connected to the internet. This will produce build/jar/accdi.jar which you can then put anywhere.
 This jar isn't redistributable under the terms of the gson license (because it doesn't contain appropriate license information) if you get around to fixing this before I do please submit a pull request.

Developing
===
 * See installing for pre-requisites.
 * $ ant resolve #This will make sure you have the required libraries in the right place for the eclipse project.
 * Using eclipse you can then import the project "from an existing git repository", it should be set up appropriately.
 NB: If you're warned about missing gson it's because ant resolve has failed.
