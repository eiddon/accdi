package net.citrusbytes.accdi.turing;

public class IncompleteTransitionFunctionException extends Exception {

	public IncompleteTransitionFunctionException(String msg) {
		super(msg);
	}

	private static final long serialVersionUID = -3264122008842415044L;
}
