package net.citrusbytes.accdi.turing;

public class TuringMachineRuntimeDescriptor {

	public final String tape;
	public final String state;
	public final int headLocation;
	
	public TuringMachineRuntimeDescriptor(String tape, String state, int headLocation){
		this.tape = tape;
		this.state = state;
		this.headLocation = headLocation;
	}
	
	@Override
	public String toString() {
		return String.format("%s%s%s", tape.substring(0, headLocation), state, tape.substring(headLocation, tape.length()));
	}
	
}
