package net.citrusbytes.accdi.render;

import java.io.FileNotFoundException;

import net.citrusbytes.accdi.turing.ClassInvariantException;
import net.citrusbytes.accdi.turing.IncompleteTransitionFunctionException;
import net.citrusbytes.accdi.turing.TuringMachine;

public class Main {

	private static final String SYSOUT = "-s";
	private static final String TEXOUT = "-l";
	
	public static final String CONF_ONLY = "-confonly";
	public static final String CONF_TOO = "-conftoo";

	public static void main(String... args) {
		if(args.length == 0) {
			usage();
		} else {
			try {
				Renderer r = null;
				TuringMachine tm = null;
				switch(args[0]){
				case SYSOUT:
					if(args.length < 2)
						usage();
					else {
						tm = new TuringMachine(args[1]);
						r = new SysoRenderer();
					}
					break;
				case TEXOUT:
					if(args.length < 3)
						usage();
					else {
						tm = new TuringMachine(args[1]);
						if(args.length == 3)
							r = new LatexFileRenderer(args[2], true, false);
						else switch(args[3]) {
						case CONF_ONLY:
							r = new LatexFileRenderer(args[2], false, true);
							break;
						case CONF_TOO:
							r = new LatexFileRenderer(args[2], true, true);
							break;
						default:
							usage();
							break;
						}
					}
					break;
				}
				
				if(tm == null || r == null) {
					usage();
				} else {
					r.render(tm.encode());

					while(!tm.isHalted())  {
						r.render(tm.step());
					}

					r.close();
				}
			} catch (ClassInvariantException e) {
				System.out.println(e.getMessage());
			} catch (IncompleteTransitionFunctionException e) {
				System.out.println(e.getMessage());
			} catch (FileNotFoundException e) {
				System.out.println(String.format("FileNotFoundException on %s", args[2]));
			} catch (AlreadyClosedException e) {
				e.printStackTrace();
			}
		}
	}

	private static void usage() {
		System.out.println(String.format("usage\taccdi %s <specifier.json>\t\t#print", SYSOUT));
		System.out.println(String.format("\taccdi %s <specifier.json> <out.tex> [-confonly|-conftoo]\t#LaTeX render", TEXOUT));
	}

}
