package net.citrusbytes.accdi.util;

public class NotACharacterException extends Exception{

	private static final long serialVersionUID = 3370670155458911610L;
	
	public NotACharacterException(String msg) {
		super(msg);
	}
}
