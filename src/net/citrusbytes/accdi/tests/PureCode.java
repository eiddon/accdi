package net.citrusbytes.accdi.tests;

import static net.citrusbytes.accdi.turing.Direction.LEFT;
import static net.citrusbytes.accdi.turing.Direction.RIGHT;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.citrusbytes.accdi.turing.ClassInvariantException;
import net.citrusbytes.accdi.turing.Delta;
import net.citrusbytes.accdi.turing.IncompleteTransitionFunctionException;
import net.citrusbytes.accdi.turing.State;
import net.citrusbytes.accdi.turing.Transition;
import net.citrusbytes.accdi.turing.TuringMachine;
import net.citrusbytes.accdi.util.NotACharacterException;
import net.citrusbytes.accdi.util.StringUtils;
import net.citrusbytes.accdi.util.UnicodeCharacter;

public class PureCode {

	public static void main(String... args) throws NotACharacterException, ClassInvariantException, IncompleteTransitionFunctionException {
		UnicodeCharacter blank = new UnicodeCharacter("˽");
		List<UnicodeCharacter> tape = StringUtils.tapeFromString("0000");
		Set<UnicodeCharacter> alphabet = StringUtils.alphabetFromString("0x");
		
		State q1 = new State("q1");
		State q2 = new State("q2");
		State q3 = new State("q3");
		State q4 = new State("q4");
		State q5 = new State("q5");
		State qaccept = new State("qacc");
		State qreject = new State("qreject");
		
		Set<State> states = new HashSet<State>();
		states.add(q1);
		states.add(q2);
		states.add(q3);
		states.add(q4);
		states.add(q5);
		states.add(qaccept);
		states.add(qreject);
		
		Set<State> accepts = new HashSet<State>();
		accepts.add(qaccept);
		
		Set<State> rejects = new HashSet<State>();
		rejects.add(qreject);
		
		State start = q1;

		UnicodeCharacter z = new UnicodeCharacter("0");
		UnicodeCharacter x = new UnicodeCharacter("x");
		
		Transition transition = new Transition(
				new Delta(q1, z, q2, blank, RIGHT),
				new Delta(q1, blank, qreject, blank, RIGHT),
				new Delta(q1, x, qreject, x, RIGHT),
				new Delta(q2, x, q2, x, RIGHT),
				new Delta(q2, blank, qaccept, blank, RIGHT),
				new Delta(q2, z, q3, x, RIGHT),
				new Delta(q3, blank, q5, blank, LEFT),
				new Delta(q3, x, q3, x, RIGHT),
				new Delta(q3, z, q4, z, RIGHT),
				new Delta(q4, blank, qreject, blank, RIGHT),
				new Delta(q4, x, q4, x, RIGHT),
				new Delta(q4, z, q3, x, RIGHT),
				new Delta(q5, z, q5, z, LEFT),
				new Delta(q5, x, q5, x, LEFT),
				new Delta(q5, blank, q2, blank, RIGHT)
				);
		TuringMachine tm = new TuringMachine(alphabet, tape, blank, states, accepts, rejects, start, transition);
		
		System.out.println(tm.encode());
		while(!tm.isHalted()) {
			System.out.println(tm.step());
		}
	}
	
}
