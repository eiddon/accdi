package net.citrusbytes.accdi.turing;

import net.citrusbytes.accdi.util.NotACharacterException;
import net.citrusbytes.accdi.util.UnicodeCharacter;

public class Delta {

	public final State sin;
	public final UnicodeCharacter cin;
	public final State sout;
	public final UnicodeCharacter cout;
	public final Direction dout;

	public Delta(State sin, UnicodeCharacter cin, State sout,
			UnicodeCharacter cout, Direction dout) {
		this.sin = sin;
		this.cin = cin;
		this.sout = sout;
		this.cout = cout;
		this.dout = dout;
	}
	
	public Delta(State sin, String cin, State sout,
			String cout, Direction dout) throws NotACharacterException {
		this.sin = sin;
		this.cin = new UnicodeCharacter(cin);
		this.sout = sout;
		this.cout = new UnicodeCharacter(cout);
		this.dout = dout;
	}
	
	@Override
	public boolean equals(Object obj) {
		if(sin == null || cin == null)
			return false;
		if(obj instanceof Delta) {
			Delta other = (Delta)obj;
			return sin.equals(other.sin) && cin.equals(other.cin);
		}
		return false;
	}

}
