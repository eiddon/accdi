package net.citrusbytes.accdi.json;

public class TuringSpecifier {
	public String tape;
	public String alphabet;
	public String blank;
	public String[] states;
	public String[] accepts;
	public String[] rejects;
	public String start;
	public String[][] deltas;
	
	public TuringSpecifier(){} //GSON.
}
