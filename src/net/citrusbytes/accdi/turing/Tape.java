package net.citrusbytes.accdi.turing;

import net.citrusbytes.accdi.util.UnicodeCharacter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Tape implements Iterable<UnicodeCharacter>{

	private ArrayList<UnicodeCharacter> internal;
	private UnicodeCharacter blank;
	private int scanHead;

	public Tape(List<UnicodeCharacter> init, UnicodeCharacter blank) {
		internal = new ArrayList<UnicodeCharacter>();
		internal.addAll(init);
		this.blank = blank;
		scanHead = 0;
	}

	public void left() {
		scanHead--;
		if(scanHead < 0)
			scanHead = 0;
	}

	public void right() {
		scanHead++;
	}

	public UnicodeCharacter scan() {
		fillBlanks();
		UnicodeCharacter ret = internal.get(scanHead);
		if(ret == null)
			return blank;
		return ret;
	}

	public void write(UnicodeCharacter uc) {
		fillBlanks();
		internal.set(scanHead, uc);
	}

	public void doInstruction(Instruction i) {
		write(i.symbol);
		switch(i.d){
		case RIGHT:
			right(); break;
		case LEFT:
			left(); break;
		}
	}
	
	private void fillBlanks() {
		ensureSize(internal, scanHead+1);
		for(int i = 0; i < internal.size(); i++) {
			if(internal.get(i) == null)
				internal.set(i, blank);
		}
	}
	
	/**
	 * @author Jon Skeet (http://stackoverflow.com/questions/7688151/java-arraylist-ensurecapacity-not-working)
	 */
	public static void ensureSize(ArrayList<?> list, int size) {
	    // Prevent excessive copying while we're adding
	    list.ensureCapacity(size);
	    while (list.size() < size) {
	        list.add(null);
	    }
	}

	@Override
	public Iterator<UnicodeCharacter> iterator() {
		return internal.iterator();
	}
	
	public int getScanHead() {
		return scanHead;
	}
	
}
