package net.citrusbytes.accdi.render;

import net.citrusbytes.accdi.turing.TuringMachineRuntimeDescriptor;

public interface Renderer {
	
	public void render(TuringMachineRuntimeDescriptor tmrd) throws AlreadyClosedException;
	public void close();
	
}
